Custom Overlay Block
-------

Custom Overlay Block allows to create a normal Drupal block that will be then displayed as an overlay. It can be closed by the user and this will be remembered in a cookie, so the overlay block will not be displayed next time.

Usage
-----

Create a normal Drupal block, assign it the CSS class "custom-overlay-block" and add it to any visible region of your theme.

In the block body, add an element with the class 
"custom-overlay-block-close", which will serve as the closing button.
For instance: 

  <a class="custom-overlay-block-close" href="#">Close</a>

At "Show block on specific pages", set the block to appear on the 
pages you want, for instance:

  <frontpage>

To deactivate a block, simply assign it to no region.

To style the block, use the class ".custom-overlay-block".
