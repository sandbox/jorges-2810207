(function ($) {

Drupal.behaviors.customOverlayBlock = {
  attach: function (context) {

   // If we already closed the block: Do nothing.
   if ($.cookie("custom_overlay_block_closed")) {
     return;
   }

   var block = $('.custom-overlay-block').first();
   block.addClass('custom-overlay-block-show');

    // Assign a click handler for the close button.
    $('.custom-overlay-block-close').click(function() {
      $('.custom-overlay-block').remove();
      $.cookie("custom_overlay_block_closed", true);
    });
  }
};

})(jQuery);

